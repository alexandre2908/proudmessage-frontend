import React from "react";
import { Link } from "react-router-dom";

import notFoundimage from "../Assets/img/success.svg";

const Resultats: React.FC<{ message: string }> = ({ message }) => {
  return (
    <div className="w-screen h-screen bg-gray-100 flex flex-col justify-center items-center">
      <img src={notFoundimage} alt="404" className="w-1/3" />

      <p className="text-lg font-display font-bold text-gray-500 mt-4">
        {message}
      </p>
      <Link
        to="/login"
        className="mt-8 cursor-pointer hover:bg-indigo-600 bg-indigo-500 py-2 px-8 rounded-lg text-gray-100 font-bold"
      >
        Se connecter
      </Link>
    </div>
  );
};
export default Resultats;
