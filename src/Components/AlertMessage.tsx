import React from "react";

type messageType = {
  message: string;
  type?: ["success", "error", "warning"];
};

const AlertMessage: React.FC<messageType> = ({ message, type }) => {
  return (
    <div
      className="bg-yellow-600 mt-2 ml-auto show_animation flex p-3 justify-center items-center w-64 rounded-md border-l-4 border-gray-500"
      style={{ right: "10px" }}
    >
      <p className="font-bold text-white tracking-wide">{message}</p>
    </div>
  );
};

export default AlertMessage;
