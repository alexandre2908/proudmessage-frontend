import React from "react";
import avatarlogin from "../Assets/img/avatarlogin.svg";
import Inputform from "./Forms/Inputform";
import { useForm } from "react-hook-form";
import { useMutation } from "@apollo/client";
import { CREATE_USER } from "../Queries/User";
import AlertMessage from "./AlertMessage";
import loadingsvg from "../Assets/img/loading.svg";
import Resultats from "./Resultats";
import { Link } from "react-router-dom";

const Register: React.FC = () => {
  type registerInput = {
    name: string;
    email: string;
    password: string;
    telephone: string;
  };

  const { register, handleSubmit, errors } = useForm<registerInput>();
  const [createUser, { data, loading, error }] = useMutation(CREATE_USER, {
    errorPolicy: "all",
  });

  const onSignupSubmit = (data_form: registerInput) => {
    createUser({
      variables: {
        email: data_form.email,
        name: data_form.name,
        telephone: data_form.telephone,
        password: data_form.password,
      },
    });
  };
  if (loading) {
    return (
      <div className="flex w-full h-screen justify-center items-center">
        <img src={loadingsvg} className="fill-current text-yellow-700" />
      </div>
    );
  }
  if (error) {
    return (
      <div className="w-screen h-screen absolute" style={{ zIndex: -1 }}>
        {error.graphQLErrors.map(({ message }, index) => (
          <AlertMessage message={message} key={index} />
        ))}
      </div>
    );
  }

  if (data) {
    return (
      <Resultats message="Votre Compte a ete cree avec succe veuillez vous connecter " />
    );
  }

  return (
    <div className="flex flex-col w-full h-screen justify-center items-center">
      <div className="w-screen h-screen absolute" style={{ zIndex: -1 }}>
        {errors.email && <AlertMessage message="email is required" />}
        {errors.password && <AlertMessage message="password is required" />}
        {errors.telephone && (
          <AlertMessage message="phone is required and minlength is 6" />
        )}
        {errors.name && (
          <AlertMessage message="username is required and minlength is 6" />
        )}
      </div>
      <img src={avatarlogin} alt="login avatar" className="w-32" />
      <form
        onSubmit={handleSubmit(onSignupSubmit)}
        className="w-3/4 lg:w-2/3 grid grid-cols-1 md:grid-cols-2 sm:col-gap-8"
      >
        <Inputform
          input_icon="fa fa-user absolute text-gray-400"
          input_type="text"
          placeholder_text="name"
          name="name"
          reference={register({ required: true, minLength: 4 })}
        />

        <Inputform
          input_icon="fa fa-envelope absolute text-gray-400"
          input_type="email"
          placeholder_text="Email"
          name="email"
          reference={register({ required: true })}
        />
        <Inputform
          input_icon="fa fa-phone absolute text-gray-400"
          input_type="text"
          placeholder_text="telephone"
          name="telephone"
          reference={register({ required: true, minLength: 4 })}
        />

        <Inputform
          input_icon="fa fa-lock absolute text-gray-400"
          input_type="password"
          placeholder_text="Password"
          name="password"
          reference={register({ required: true, minLength: 6 })}
        />

        <button
          type="submit"
          className=" mt-8 text-white bg-green-400 focus:outline-none hover:bg-green-500 py-2 px-3 uppercase font-bold font-diplay rounded-full flex items-center justify-center mr-2"
        >
          <i className="fab fa-2x fa-telegram-plane mr-4"></i>
          Register
        </button>
        <div className="ml-auto mt-2">
          <p className="inline text-white">Vous avez deja un compte? </p>
          <Link className="underline text-green-500 text-lg" to="/login">
            Se Connecter
          </Link>
        </div>
      </form>
    </div>
  );
};

export default Register;
