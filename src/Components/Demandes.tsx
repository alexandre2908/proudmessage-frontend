import React from "react";
import avatarlogin from "../Assets/img/avatarlogin.svg";
import { useQuery, useMutation } from "@apollo/client";
import { ALL_DEMANDES_BY_USER, UPDATE_DEMANDES } from "../Queries/Demandes";
import { getConnectedUser } from "./Authentification";
import loadingsvg from "../Assets/img/loading.svg";
import AlertMessage from "./AlertMessage";
import NavBar from "../Components/Navbar";
import LogoComp from "../Components/Logo";

const Demandes = () => {
  const currentUser = getConnectedUser();
  const { error, loading, data, refetch } = useQuery(ALL_DEMANDES_BY_USER, {
    variables: { _id: getConnectedUser()?.userId },
    errorPolicy: "all",
    pollInterval: 2000,
  });

  const [updateUser, { error: updateEror, data: DemandeData }] = useMutation(
    UPDATE_DEMANDES
  );

  if (updateEror) {
    console.log(updateEror);
  }
  if (DemandeData) {
    refetch();
  }

  if (error) {
    error.graphQLErrors.map(({ message }, index) => {
      console.log(message);
    });
  }
  return (
    <div>
      <div className="flex justify-between pt-6 px-8 items-center">
        <LogoComp />
        <NavBar />
      </div>
      <div className="mt-4 px-24">
        {DemandeData ? (
          <AlertMessage message="La demande a ete mise a jours" />
        ) : (
          ""
        )}
        <h1 className="text-center text-secondary font-display text-3xl font-bold">
          Mes Demandes
        </h1>
        <div className="grid grid-cols-1 sm:grid-cols-2 lg:grid-cols-3 gap-4 mt-8">
          {loading ? (
            <div className="flex w-full h-screen justify-center items-center absolute">
              <img src={loadingsvg} className="fill-current text-yellow-700" />
            </div>
          ) : (
            data.UserDemandes.map((demandes: any) => (
              <div
                className="lg:flex bg-white rounded-lg p-4"
                key={demandes._id}
              >
                <img
                  src={avatarlogin}
                  className="h-16 w-16 lg:h-24 lg:w-24 rounded-full mx-auto lg:mx-0 lg:mr-6"
                />
                <div className="text-center lg:text-left">
                  <h2 className="text-lg capitalize">
                    {demandes.from._id === currentUser?.userId
                      ? demandes.to.name
                      : demandes.from.name}
                  </h2>
                  <div className="text-purple-500">{demandes.etat}</div>
                  <div className="text-gray-600">
                    {" "}
                    {demandes.from._id === currentUser?.userId
                      ? demandes.to.email
                      : demandes.from.email}
                  </div>
                  <div className="text-gray-600">
                    {demandes.from._id === currentUser?.userId
                      ? demandes.to.telephone
                      : demandes.from.telephone}
                  </div>
                  {demandes.from._id === currentUser?.userId ||
                  demandes.etat !== "En cours" ? (
                    ""
                  ) : (
                    <div className="flex justify-between">
                      <button
                        onClick={() =>
                          updateUser({
                            variables: { _id: demandes._id, etat: "accepted" },
                          })
                        }
                        className="bg-green-400 text-white px-4 py-2 shadow-lg transition-all duration-500 hover:shadow-none"
                      >
                        <i className="fa fa-check"></i>
                      </button>

                      <button
                        onClick={() =>
                          updateUser({
                            variables: { _id: demandes._id, etat: "refused" },
                          })
                        }
                        className="bg-red-400 text-white px-4 py-2 shadow-lg transition-all duration-500 hover:shadow-none"
                      >
                        <i className="fa fa-times"></i>
                      </button>
                    </div>
                  )}
                </div>
              </div>
            ))
          )}
        </div>
      </div>
    </div>
  );
};

export default Demandes;
