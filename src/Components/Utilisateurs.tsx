import React from "react";

import AlertMessage from "./AlertMessage";
import NavBar from "../Components/Navbar";
import LogoComp from "../Components/Logo";
import avatarlogin from "../Assets/img/avatarlogin.svg";
import { useQuery, useMutation } from "@apollo/client";
import { ALL_USERS } from "../Queries/User";
import { CREATE_DEMANDE } from "../Queries/Demandes";
import loadingsvg from "../Assets/img/loading.svg";
import { getConnectedUser } from "./Authentification";

const Utilisateurs = () => {
  const connectedUser = getConnectedUser();
  const { error, data, loading, refetch } = useQuery(ALL_USERS, {
    errorPolicy: "all",
  });
  const [
    createDemande,
    { data: demandeData, loading: loadingDemande, error: errorDemande },
  ] = useMutation(CREATE_DEMANDE, {
    errorPolicy: "all",
  });

  if (demandeData) {
    refetch();
  }

  return (
    <div>
      <div className="flex justify-between pt-6 px-8 items-center">
        <LogoComp />
        <NavBar />
      </div>
      <div className="mt-4 px-24">
        {errorDemande
          ? errorDemande.graphQLErrors.map(({ message }, i) => (
              <AlertMessage message={message} />
            ))
          : ""}
        <h1 className="text-center text-secondary font-display text-3xl font-bold">
          Users
        </h1>
        <div className="grid grid-cols-1 sm:grid-cols-2 lg:grid-cols-3 gap-4 mt-8">
          {loading ? (
            <div className="flex w-full h-screen justify-center items-center absolute">
              <img src={loadingsvg} className="fill-current text-yellow-700" />
            </div>
          ) : (
            data.Users.map((user: any) =>
              user._id !== connectedUser?.userId ? (
                <div className="lg:flex bg-white rounded-lg p-4" key={user._id}>
                  <img
                    src={avatarlogin}
                    className="h-16 w-16 lg:h-24 lg:w-24 rounded-full mx-auto lg:mx-0 lg:mr-6"
                  />
                  <div className="text-center lg:text-left">
                    <h2 className="text-lg capitalize">{user.name}</h2>
                    <div className="text-gray-600">{user.email}</div>
                    <div className="text-gray-600">{user.telephone}</div>

                    <button
                      onClick={() => {
                        createDemande({
                          variables: {
                            from: connectedUser?.userId,
                            to: user._id,
                          },
                        });
                      }}
                      className="bg-green-400 text-white px-4 py-2 shadow-lg transition-all duration-500 hover:shadow-none"
                    >
                      <i className="fa fa-check mr-2"></i>
                      Effectuer Demande
                    </button>
                  </div>
                </div>
              ) : (
                ""
              )
            )
          )}
          ,
        </div>
      </div>
    </div>
  );
};

export default Utilisateurs;
