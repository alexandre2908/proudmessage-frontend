import React, { Fragment } from "react";
import { Link, Redirect, useHistory } from "react-router-dom";
import { getConnectedUser, logout } from "./Authentification";

const NavBar: React.FC = () => {
  const currentUser = getConnectedUser();
  const history = useHistory();
  const deconnect = () => {
    logout();
    history.push("/login");
  };
  return (
    <nav className=" w-2/3 md:w-1/2 lg:w-1/3 flex justify-between">
      <Link
        to="/chat"
        className="text-gray-500  hover:text-secondary transition-all duration-500"
      >
        Chats
      </Link>
      <Link
        to="/users"
        className="text-gray-500  hover:text-secondary transition-all duration-500"
      >
        Utilisateurs
      </Link>

      <Link
        to="/demandes"
        className="text-gray-500  hover:text-secondary transition-all duration-500"
      >
        Demandes
      </Link>

      <Link
        to="/chat"
        className="text-gray-500  hover:text-green-500 transition-all duration-500 uppercase font-bold"
      >
        {currentUser?.name}
      </Link>
      <button
        onClick={deconnect}
        className="text-gray-500 hover:text-secondary transition-all duration-500"
      >
        Logout
      </button>
    </nav>
  );
};

export default NavBar;
