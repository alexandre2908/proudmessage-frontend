import React from "react";
import logo from "../Assets/img/comment.png";

const LogoComp: React.FC = () => {
  return (
    <div>
      <img className=" w-8 sm:w-12 sm:h-12 inline" src={logo} alt="logo" />
      <h1 className="inline text-white font-bold text-smtext-sm ml-3">
        Proud message
      </h1>
    </div>
  );
};

export default LogoComp;
