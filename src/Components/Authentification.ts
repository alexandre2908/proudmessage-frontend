const user = localStorage.getItem("currentUser");
let isLoggedin = false;

type userData = {
  name?: string;
  token: string;
  userId: string;
};

function isAuthentificated() {
  if (user) {
    isLoggedin = true;
  }
  return isLoggedin;
}

const authentificate = (data: any) => {
  localStorage.setItem("currentUser", JSON.stringify(data));
  isLoggedin = true;
};

const getConnectedUser = () => {
  if (user) {
    const currentUser: userData = JSON.parse(user);
    return currentUser;
  }
};

const logout = () => {
  localStorage.removeItem("currentUser");
  isLoggedin = false;
};

export { isAuthentificated, logout, getConnectedUser, authentificate };
