import React from "react";
import { Link } from "react-router-dom";
import { useQuery } from "@apollo/client";
import { LASTMESSAGE } from "../Queries/Messages";
import { getConnectedUser } from "./Authentification";

type contactData = {
  chatId: string;
  name?: string;
  contact_image?: any;
};

const Contact: React.FC<contactData> = ({ chatId, contact_image, name }) => {
  const currentUser = getConnectedUser();
  const { error, data, loading } = useQuery(LASTMESSAGE, {
    errorPolicy: "all",
    variables: {
      from: currentUser?.userId,
      to: chatId,
    },
    pollInterval: 5000,
  });
  return (
    <div>
      <div className="grid grid-cols-5 sm:grid-cols-1 lg:grid-cols-5 col-gap-2 items-center">
        <div className="relative sm:col-span-3 lg:col-span-1 sm:self-center">
          <img
            src={contact_image}
            alt="avatars"
            className="w-16 rounded-full sm:mx-auto"
          />
          <i
            className="fa fa-circle absolute text-green-500"
            style={{ bottom: ".1rem", right: "5px" }}
          ></i>
        </div>
        <div className="col-span-3 sm:col-span-2 lg:col-span-3 border-gray-100">
          <Link
            to={"/chat/".concat(chatId)}
            className="text-gray-300 font-bold text-left sm:text-center lg:text-left"
          >
            {name}
          </Link>
          <p className="font-thin text-gray-500 text-left sm:text-center lg:text-left">
            {error
              ? error.graphQLErrors.map(({ message }, index) => message)
              : data
              ? data.GetLastMessage.message
              : ""}
            {loading ? "Chargement......." : ""}
          </p>
        </div>
        <p className="text-gray-300 text-base">
          {error
            ? error.graphQLErrors.map(({ message }, index) => message)
            : data
            ? new Date(
                parseInt(data.GetLastMessage.createdAt)
              ).toLocaleTimeString()
            : ""}
        </p>
      </div>
    </div>
  );
};

export default Contact;
