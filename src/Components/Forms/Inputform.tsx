import React from "react";

type Inputconfig = {
  input_type: string;
  placeholder_text: string;
  input_icon: string;
  name: string;
  reference?: any;
};

const Inputform: React.FC<Inputconfig> = ({
  input_icon,
  input_type,
  placeholder_text,
  name,
  reference,
}) => {
  return (
    <div className="relative w-full mt-8">
      <i className={input_icon} style={{ top: ".8rem", left: "1rem" }}></i>
      <input
        placeholder={placeholder_text}
        type={input_type}
        className=" inline pl-12 py-2 text-white w-full rounded-full bg-primarylight placeholder-gray-400 focus:outline-none focus:shadow-outline"
        name={name}
        ref={reference}
      />
    </div>
  );
};

export default Inputform;
