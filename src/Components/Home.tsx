import React from "react";
import logo from "../Assets/img/comment.png";
import { Link } from "react-router-dom";

const HomeComponent: React.FC = () => {
  return (
    <div className="w-screen h-screen flex flex-col justify-center items-center">
      <div>
        <h1 className="inline text-3xl text-white mr-4">welcome to </h1>
        <img className=" w-8 sm:w-12 sm:h-12 inline" src={logo} alt="logo" />
        <h1 className="inline text-white font-bold text-smtext-sm ml-3">
          Proud message
        </h1>
      </div>
      <Link
        to="/chat"
        className="px-6 mt-4 py-3 block bg-green-500 text-white text-2xl rounded-lg"
      >
        Start Chat
      </Link>
    </div>
  );
};

export default HomeComponent;
