import React from "react";
import { Link } from "react-router-dom";

import notFoundimage from "../Assets/img/404.svg";

export default function NotFound() {
  return (
    <div className="w-screen h-screen bg-gray-100 flex flex-col justify-center items-center">
      <img src={notFoundimage} alt="404" className="w-1/2" />
      <Link
        to="/"
        className="mt-8 cursor-pointer hover:bg-indigo-600 bg-indigo-500 py-2 px-8 rounded-lg text-gray-100 font-bold"
      >
        Go to the main page
      </Link>
    </div>
  );
}
