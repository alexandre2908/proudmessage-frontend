import React from "react";

type MessagesProp = {
  message_text: string;
  isMe: boolean;
  message_time: string;
};

const Message: React.FC<MessagesProp> = ({
  message_text,
  isMe,
  message_time,
}) => {
  if (!isMe) {
    return (
      <div className="sm:mt-4 md:mt-0">
        <p className="inline-block bg-primarydark p-3 rounded-full text-gray-300 sm:text-sm lg:text-base">
          {message_text}
        </p>
        <p className="mx-auto text-gray-500">{message_time}</p>
      </div>
    );
  } else {
    return (
      <div className="sm:mt-4 md:mt-0 w-2/3 ml-auto text-right">
        <p
          className="inline-block bg-secondary p-3 rounded-full text-gray-300  sm:text-sm lg:text-base"
          style={{ wordBreak: "break-all" }}
        >
          {message_text}
        </p>
        <p className="mx-auto text-gray-500">{message_time}</p>
      </div>
    );
  }
};

export default Message;
