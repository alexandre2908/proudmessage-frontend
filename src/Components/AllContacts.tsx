import React from "react";
import Contact from "./Contact";
import avatarlogin from "../Assets/img/avatarlogin.svg";

type contactData = [
  {
    _id: string;
    name?: string;
    emal?: any;
    telephone?: string;
  }
];

// const alldata: contactData[] = [
//   {
//     contact_name: "Jessica Woe",
//     contact_image: avatar1,
//     last_message: "bonjour comment tu vas?",
//     message_time: "9:00",
//   },
//   {
//     contact_name: "Axel Mwenze",
//     contact_image: avatar2,
//     last_message: "bonjour comment tu vas?",
//     message_time: "10:00",
//   },
//   {
//     contact_name: "Donna Poolsen",
//     contact_image: avatar3,
//     last_message: "Je ne te voix pas",
//     message_time: "7:00",
//   },
//   {
//     contact_name: "Jesssica Pearson",
//     contact_image: avatar4,
//     last_message: "ou es-tu?",
//     message_time: "19:00",
//   },
//   {
//     contact_name: "Louis Lit",
//     contact_image: avatar5,
//     last_message: "d'accord",
//     message_time: "20:00",
//   },
// ];

const AllContacts: React.FC<{ allContacts: contactData }> = ({
  allContacts,
}) => {
  return (
    <div>
      {allContacts.map((cont, index) => (
        <Contact
          key={index}
          chatId={cont._id}
          contact_image={avatarlogin}
          name={cont.name}
        />
      ))}
    </div>
  );
};

export default AllContacts;
