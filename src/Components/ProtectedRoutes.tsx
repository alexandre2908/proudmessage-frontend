import React, { Component } from "react";
import { isAuthentificated } from "./Authentification";
import { Route, Redirect, RouteProps } from "react-router-dom";

const ProtectedRoutes = ({ component: Component, path }: RouteProps) => {
  if (!isAuthentificated()) {
    return <Redirect to="/login" />;
  }
  return <Route component={Component} path={path} />;
};

export default ProtectedRoutes;
