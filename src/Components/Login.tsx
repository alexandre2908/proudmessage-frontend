import React from "react";
import avatarlogin from "../Assets/img/avatarlogin.svg";
import Inputform from "./Forms/Inputform";
import { LOGIN_USER } from "../Queries/User";
import { useQuery, useMutation } from "@apollo/client";
import { useForm } from "react-hook-form";
import accessdenied from "../Assets/img/accessdenied.svg";
import loadingsvg from "../Assets/img/loading.svg";
import { Link, Redirect } from "react-router-dom";
import AlertMessage from "./AlertMessage";
import { authentificate, isAuthentificated } from "./Authentification";

type InputfForm = {
  email: string;
  password: string;
};

const Login: React.FC = () => {
  const { register, handleSubmit, watch, errors } = useForm<InputfForm>();
  const [loginuser, { data, loading, error }] = useMutation(LOGIN_USER, {
    errorPolicy: "all",
  });
  function refreshPage() {
    window.location.reload(false);
  }

  const formSubmition = (data: InputfForm) => {
    loginuser({ variables: { email: data.email, password: data.password } });
  };

  if (error) {
    return (
      <div className="flex flex-col h-screen w-full justify-center items-center">
        <p className="text-red-500 text-display text-2xl font-light tracking-wide">
          Username or password incorrect please
        </p>
        <button
          onClick={refreshPage}
          className=" mt-8 text-white bg-green-400 focus:outline-none hover:bg-green-500 py-2 px-3 uppercase font-bold font-diplay rounded-full flex items-center justify-center mr-2"
        >
          Try Again
        </button>
      </div>
    );
  }

  if (loading) {
    return (
      <div className="flex w-full h-screen justify-center items-center">
        <img src={loadingsvg} className="fill-current text-yellow-700" />
      </div>
    );
  }

  if (data) {
    authentificate(data.Login);
    if (isAuthentificated()) {
      window.location.assign("/");
    }
  }

  return (
    <div className="flex flex-col w-full h-screen justify-center items-center">
      <div className="w-screen h-screen absolute">
        {errors.email && <AlertMessage message="email is requierd" />}
        {errors.password && <AlertMessage message="password is requierd" />}
      </div>
      <img src={avatarlogin} alt="login avatar" className="w-32" />
      <form
        className="w-3/4 lg:w-1/3 flex flex-col items-center transform"
        onSubmit={handleSubmit(formSubmition)}
      >
        <Inputform
          input_icon="fa fa-user absolute text-gray-400"
          input_type="text"
          placeholder_text="Email"
          name="email"
          reference={register({ required: true, minLength: 4 })}
        />
        <Inputform
          input_icon="fa fa-lock absolute text-gray-400"
          input_type="password"
          placeholder_text="Password"
          name="password"
          reference={register({ required: true, minLength: 6 })}
        />
        <div className="ml-auto mt-2">
          <p className="inline text-white">Vous n'avez pas de comptes? </p>
          <Link to="/register" className="underline text-green-500 text-lg">
            S'enregister
          </Link>
        </div>
        <button
          type="submit"
          className=" mt-8 text-white bg-green-400 focus:outline-none hover:bg-green-500 py-2 px-3 uppercase font-bold font-diplay rounded-full flex items-center justify-center mr-2"
        >
          <i className="fab fa-2x fa-telegram-plane mr-4"></i>
          Login
        </button>
      </form>
    </div>
  );
};

export default Login;
