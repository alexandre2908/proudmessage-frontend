import React from "react";

type SearchData = {
  search_data: [];
};

const ContactSearch: React.FC<SearchData> = () => {
  return (
    <div className="rounded-lg  mt-4 sm:mt-0">
      <div className="relative">
        <i
          className="fa fa-search absolute text-gray-400"
          style={{ top: ".8rem", left: "1rem" }}
        ></i>
        <input
          placeholder="Search"
          className="pl-12 py-2 text-white uppercase w-full rounded-full bg-primarylight placeholder-gray-400 focus:outline-none focus:shadow-outline"
        />
      </div>
    </div>
  );
};

export default ContactSearch;
