import React from "react";
import { useQuery, useMutation } from "@apollo/client";
import { MESSAGEBYUSERS, ADD_MESSAGE } from "../Queries/Messages";
import { useParams } from "react-router-dom";
import { getConnectedUser } from "./Authentification";
import Message from "./Message";
import { useForm } from "react-hook-form";

const Conversations: React.FC = () => {
  type MessageInput = {
    message: String;
  };

  const { chatId } = useParams();
  const currentUser = getConnectedUser();

  const { register, handleSubmit, errors, reset } = useForm<MessageInput>();
  const { data, error, loading, refetch } = useQuery(MESSAGEBYUSERS, {
    errorPolicy: "all",
    variables: { from: chatId, to: currentUser?.userId },
    pollInterval: 2000,
  });

  const [
    addMessage,
    { data: addData, error: Adderror, loading: addLoading },
  ] = useMutation(ADD_MESSAGE, { errorPolicy: "all" });

  const onMessageSend = (data_form: MessageInput) => {
    addMessage({
      variables: {
        from: currentUser?.userId,
        to: chatId,
        message: data_form.message,
      },
    });

    reset({ message: "" });

    refetch();
  };

  return (
    <div
      className="hidden sm:grid col-span-2 bg-primarylight grid grid-rows-4 rounded-lg relative py-4 px-8 sm:px-4 lg:px-8"
      style={{ height: "85vh" }}
    >
      <div className="row-span-3 flex flex-col w-fullh-3/4 overflow-auto px-2">
        {data
          ? data.MessageByUsers.map((messages: any) => (
              <Message
                key={messages._id}
                message_text={messages.message}
                message_time={new Date(
                  parseInt(messages.createdAt)
                ).toLocaleTimeString()}
                isMe={messages.from._id === currentUser?.userId ? true : false}
              />
            ))
          : "Chargement"}
      </div>
      <div className="self-end">
        <form className=" flex" onSubmit={handleSubmit(onMessageSend)}>
          <input
            className="flex-1 mr-8 rounded-full pl-6  text-gray-700 focus:outline-none focus:shadow-outline"
            placeholder="Type your message"
            minLength={4}
            required
            name="message"
            ref={register({ required: true, minLength: 4 })}
          />
          <button
            type="submit"
            className="text-white bg-green-400 py-2 px-3 rounded-full"
          >
            <i className="fab fa-2x fa-telegram-plane"></i>{" "}
          </button>
        </form>
      </div>
    </div>
  );
};

export default Conversations;
