import { gql } from "@apollo/client";

const GET_ALL_CONTACT = gql`
  {
    contacts {
      id
      name
      email
      photo
    }
  }
`;

const GET_ONE_CONTACT = gql`
    {
        contact($id: String!){
        id
        name
        email
        photo
        }
    }
`;

export { GET_ALL_CONTACT, GET_ONE_CONTACT };
