import { gql } from "@apollo/client";

const CREATE_USER = gql`
  mutation createUser(
    $name: String!
    $email: String!
    $telephone: String!
    $password: String!
  ) {
    createUser(
      name: $name
      email: $email
      telephone: $telephone
      password: $password
    ) {
      name
      email
      telephone
      createdAt
    }
  }
`;

const LOGIN_USER = gql`
  mutation LoginUser($email: String!, $password: String!) {
    Login(email: $email, password: $password) {
      name
      userId
      token
    }
  }
`;

const ALL_USERS = gql`
  {
    Users {
      _id
      name
      email
      telephone
    }
  }
`;

const USER_INFO = gql`
  query User($_id: String!) {
    User(_id: $_id) {
      _id
      name
      email
      telephone
      demandes {
        etat
      }
      contacts {
        _id
        name
        email
        telephone
      }
    }
  }
`;

export { CREATE_USER, LOGIN_USER, ALL_USERS, USER_INFO };
