import { gql } from "@apollo/client";

const ALL_DEMANDES_BY_USER = gql`
  query Demandes($_id: String!) {
    UserDemandes(_id: $_id) {
      _id
      etat
      from {
        _id
        name
        email
        telephone
      }
      to {
        _id
        name
        email
        telephone
      }
    }
  }
`;

const UPDATE_DEMANDES = gql`
  mutation updateUser($_id: String!, $etat: String!) {
    updateDemande(_id: $_id, etat: $etat) {
      etat
      from {
        _id
        name
        email
        telephone
      }
      to {
        _id
        name
        email
        telephone
      }
    }
  }
`;

const CREATE_DEMANDE = gql`
  mutation CreateDemande($from: String!, $to: String!) {
    createDemande(from: $from, to: $to) {
      etat
      from {
        _id
        name
      }
      to {
        _id
        name
      }
    }
  }
`;

export { ALL_DEMANDES_BY_USER, UPDATE_DEMANDES, CREATE_DEMANDE };
