import { gql } from "@apollo/client";
import React from "react";

const MESSAGEBYUSERS = gql`
  query MessageByUsers($from: String!, $to: String!) {
    MessageByUsers(from: $from, to: $to) {
      _id
      message
      createdAt
      from {
        _id
      }
      to {
        _id
      }
    }
  }
`;

const LASTMESSAGE = gql`
  query GetLastMessage($from: String!, $to: String!) {
    GetLastMessage(from: $from, to: $to) {
      _id
      message
      createdAt
    }
  }
`;

const ADD_MESSAGE = gql`
  mutation addMessage($from: String!, $to: String!, $message: String!) {
    addMessage(from: $from, to: $to, message: $message) {
      _id
      message
      createdAt
    }
  }
`;

export { MESSAGEBYUSERS, ADD_MESSAGE, LASTMESSAGE };
