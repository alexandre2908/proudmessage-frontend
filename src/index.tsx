import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./App";
import Login from "./Components/Login";
import * as serviceWorker from "./serviceWorker";
import {
  ApolloClient,
  HttpLink,
  InMemoryCache,
  ApolloProvider,
  from,
} from "@apollo/client";
import {
  Route,
  BrowserRouter as Router,
  Switch,
  Redirect,
} from "react-router-dom";
import Register from "./Components/Register";
import NotFound from "./Components/NotFound";
import ProtectedRoutes from "./Components/ProtectedRoutes";
import Demandes from "./Components/Demandes";
import {
  isAuthentificated,
  getConnectedUser,
} from "./Components/Authentification";
import Utilisateurs from "./Components/Utilisateurs";
import HomeComponent from "./Components/Home";

const apoloClient = new ApolloClient({
  cache: new InMemoryCache(),
  link: new HttpLink({
    uri: process.env.REACT_APP_API_URL,
    headers: {
      authorization: isAuthentificated() ? getConnectedUser()?.token : "",
    },
  }),
});

const Application = () => (
  <ApolloProvider client={apoloClient}>
    <Router>
      <Switch>
        <Route exact path="/" component={HomeComponent} />
        <ProtectedRoutes path="/chat/:chatId?" component={App} />
        <ProtectedRoutes path="/demandes" component={Demandes} />
        <ProtectedRoutes path="/users" component={Utilisateurs} />

        <Route path="/login" component={Login} />
        <Route path="/register" component={Register} />
        <Route path="*" component={NotFound} />
      </Switch>
    </Router>
  </ApolloProvider>
);

ReactDOM.render(
  <React.StrictMode>
    <Application />
  </React.StrictMode>,
  document.getElementById("root")
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
