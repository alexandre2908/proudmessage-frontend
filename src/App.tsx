import React from "react";
import Contact from "./Components/Contact";
import { USER_INFO } from "./Queries/User";
import AllContacts from "./Components/AllContacts";
import ContactSearch from "./Components/ContactSearch";
import NavBar from "./Components/Navbar";
import LogoComp from "./Components/Logo";
import Conversations from "./Components/Conversations";
import { getConnectedUser } from "./Components/Authentification";
import { useQuery } from "@apollo/client";
import { useParams } from "react-router-dom";

function App() {
  const currentUser = getConnectedUser();
  const { chatId } = useParams();

  const { error, loading, data } = useQuery(USER_INFO, {
    variables: { _id: currentUser?.userId },
    errorPolicy: "all",
  });

  return (
    <div>
      <div className="flex justify-between pt-6 px-8 items-center">
        <LogoComp />
        <NavBar />
      </div>
      <div className="mt-4 sm:mt-2 w-full h-full grid sm:grid-cols-3 sm:col-gap-4 lg:col-gap-12 row-gap-6 py-2 px-12 sm:px-4 lg:px-12">
        <div className="bg-primarylight rounded-lg p-3">
          {data ? <AllContacts allContacts={data.User.contacts} /> : ""}
        </div>
        {chatId ? (
          <Conversations />
        ) : data ? (
          <h1 className="text-gray-300 text-2xl text-center ">
            Bienvenue cher:
            <span className="text-green-500 uppercase">{data.User.name}</span>
          </h1>
        ) : (
          ""
        )}
      </div>
    </div>
  );
}

export default App;
