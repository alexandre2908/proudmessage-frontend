module.exports = {
  purge: {
    enabled: true,
    content: ["./src/**/*.html", "./src/**/*.tsx", "./public/**/*.html"],
  },
  theme: {
    extend: {
      colors: {
        "regal-blue": "#243c5a",
        primarydark: "#4d426d",
        primarylight: "#5c4f82",
        secondary: "#efa985",
      },
    },
    fontFamily: {
      firstfont: ["Open Sans", "sans-serif"],
      secondfont: ["Roboto", "sans-serif"],
    },
  },
  variants: {},
  plugins: [],
};
